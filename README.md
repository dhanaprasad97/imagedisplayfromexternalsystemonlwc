# File Retrieve to Salesforce from External System using Lightning Web Components and REST APIs

This asset helps to retrieve an image (or) file from external system to salesforce system using lightning web components and rest apis


### Package Development Model

This Asset contains of lightning web component that displays image and apex class to call external system using REST APIs which is called by LWC to retrieve image