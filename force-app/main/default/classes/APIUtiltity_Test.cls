/*
* Created by : Dhana Prasad
* Test class for APIUtility class
*/

@isTest
public class APIUtility_Test {

    //test class for getResponse method
    @isTest 
    public static void getResponseTest() {
        Test.setMock(HttpCalloutMock.class, new HttpMockController()); 
        HttpResponse response = APIUtility.getResponse('Url');
        String expectedValue = '{"example":"test"}';
        System.assertEquals(response.getBody(), expectedValue); 
    }

    //test class for getResponse method with exception as output
    @isTest 
    public static void getResponseExceptionTest() {   
        try{
            APIUtility.getResponse('URL');
        }Catch(Exception e){
            System.debug(e.getMessage());
                System.assert(e.getMessage().contains('do not support Web service callouts'));
        }
    }

    
}