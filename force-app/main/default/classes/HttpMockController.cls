/*
 * Created by : Dhana Prasad
 * contains mock callout function for Http requests in test class
 */
@isTest
global class HttpMockController implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest request) {
        HTTPResponse response = new HTTPResponse();
        response.setBody('{"example":"test"}');
        response.setHeader('Content-Type', 'application/json');
        response.setStatusCode(200);
        return response; 
    }
}