/*
 * Created by : Dhana Prasad
 * test class for ImageFromApi
 */
@isTest
public with sharing class ImageFromApi_Test {

    //test method for successful image retreive
    @isTest 
    public static void getImageTest() {
        Test.setMock(HttpCalloutMock.class, new HttpMockController()); 
        String response = ImageFromApi.getImage('1234');
        System.assertEquals(true, response.contains('base64'));
    }

     //test method for failed image retreive
    @isTest 
    public static void getImageExceptionTest() {   
        try{
            ImageFromApi.getImage('1234');
        }Catch(Exception e){
            System.assertEquals(true, e.getMessage().contains('Script-thrown exception'));
        }
    }

}
