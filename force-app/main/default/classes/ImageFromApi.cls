//Create by Dhana Prasad
//Class is used to retrieve image from external system and send it to LWC
public with sharing class ImageFromApi {

    // helps to retrive image from external system
    @AuraEnabled
    public static String getImage(String cardholderId){
      String imageUrl = null;
  
       
        try{
        String apiURL = Label.ORCA_APIUrl+'/api/Cardholders/'+cardholderId+'/Image';
        //sending api url to API utility method that calls the rest api and sends back image data
        HttpResponse res = APIUtility.getResponse(apiURL);

        
        Blob image = res.getBodyAsBlob();

        //Next set the image url. The type of image in my case is determined by the webservice response
        imageUrl = 'data:'+res.getHeader('Content-Type')+';base64,'+EncodingUtil.base64Encode(image);
        }
        catch(exception e) {
          //Handling exception
          throw new AuraHandledException('Error while fetching cardholder image ---> '+e.getMessage());
      }
        
        return imageUrl;
    }
}
