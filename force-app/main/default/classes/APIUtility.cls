/*
 * Created by : Dhana Prasad
 * class contains utility methods to calll external system
 */
public with sharing class APIUtility {

    public static Http http = new Http();
    public static HttpRequest req = new HttpRequest();
    public static HttpResponse res = new HttpResponse();

    //GET
    public static HttpResponse getResponse(String strURL) {
        
        apiCallSettings();
        req.setMethod('GET');
        req.setEndpoint(strURL);
        res = http.send(req);
        return res;
    }

    public static void apiCallSettings() {
        String username = 'Username';
        String password = 'Password'; 
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic  ' + EncodingUtil.base64Encode(headerValue);             
        req.setHeader('Authorization', authorizationHeader);
    }
}
