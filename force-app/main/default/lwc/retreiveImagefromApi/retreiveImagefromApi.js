import { LightningElement, api, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getImageData from "@salesforce/apex/ImageFromApi.getImage";

export default class RetreiveImagefromApi extends LightningElement {
    @track isLoading = true;
    @track imageURL;

    connectedCallback() {
        this.getCardholderImage();
    }

    getCardholderImage() {
        this.isLoading = true;
        getImageData({
            cardholderId: '123'
        })
            .then(result => {
                this.imageURL = result;
                console.log("RESULT After  download Image:::" + result);
                this.isLoading = false;
            })
            .catch(error => {
                this.isLoading = false;
                console.log("Error while saving uploaded image " + error);
            });
    }

}